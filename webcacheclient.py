import base64
import bz2
import json
import os
import pickle
from os.path import expanduser

import furl
import requests


# add constructor to set webcache location
# programmatically. fall back to config if no explicit location provided
class WebCacheClient:
    WEBCACHE_LOCATION = "localhost:9011"

    def __init__(self):
        # it's probably better to specify the webcache
        # IP in the file rather than the env name
        expected_env_location = "%s/.labscape.env" % expanduser("~")
        if os.path.exists(expected_env_location):
            with open(expected_env_location, "r") as fi:
                content = fi.read()
                if content.strip().lower() == "dev":
                    print("using DEV-environment for cache")
                    self.WEBCACHE_LOCATION = "localhost:9011"

                if content.strip().lower() == "docker":
                    print("using docker-environment for cache")
                    self.WEBCACHE_LOCATION = "webcache:9011"

    def get_proxy_list(self, num_proxies: int = 1000):
        """
        gets list of proxies from data service. Some of the proxies might
        not work, but the probability of having a
        majority of good proxies is rather high.
        :param num_proxies: maximal number of proxies returned (the higher the
        number of proxies, the larger the share of non-working proxies. Usually
        you can expect there to be around 1500 working proxies in the service at
        any given time)
        :return: list of proxies
        """
        if type(num_proxies) != int:
            raise ValueError("num_proxies must be an integer")

        if num_proxies < 1:
            return []
        else:
            service_url = "http://%s/proxies/%s" % \
                          (self.WEBCACHE_LOCATION, num_proxies)
            data = requests.get(service_url).json()
            if data is not None and "response" in data:
                return data["response"]
            else:
                raise ValueError("could not get proxies: %s" % data)

    def fetch_urls(self, url_list, category: str,
                   output, method="GET", max_age_days=360):
        """
        uses data service to fetch a list of urls

        :param url_list: non-empty list of url's to be obtained. if data is
        included in a POST request -> list of tuples(url, data-json)
        :param category: name of the dataprocessor issuing request and type of
        request. Example: "dataprocessor_geocoder:find-latlon".
        Only for logging purposes
        :param output: how should the cache output be interpreted and serialised
        supported are JSON and XML (BeautifulSoup object returned)
        :param method: GET/POST
        :param max_age_days: maximum age of page in cache in days. If a URL
        has been cached longer ago than these days, it is fetched again
        :return: dictionary where input url's are mapped to cache-result.
        available fields in cache-result-dict:
        content, size, url, format, creation_date, url_key
        """
        if output.lower() not in ["json", "xml"]:
            raise ValueError("output-field must be either JSON or XML")

        if method.upper() not in ["GET", "POST"]:
            raise ValueError(
                "the web cache currently only supports GET and POST Requests")

        filtered_url_list = []
        for url_item in url_list:
            url_tuple = (url_item, '{}') if type(url_item) is str else url_item

            if is_valid_url(url_tuple[0]):
                filtered_url_list.append(url_tuple)
            else:
                print("invalid URL supplied to cache: %s. will ignore it" %
                      url_tuple[0])

        service_url = "http://%s/fetch/%s/%s/%s/%s" % \
                      (self.WEBCACHE_LOCATION, max_age_days,
                       category, output, method)

        if any('localhost' in url[0] or '127.0.0.1' in url[0]
               for url in filtered_url_list):
            data = {}
            for url in filtered_url_list:
                data[url[0]] = {'content': requests.get(url[0]).json()}
            return data
        else:
            data = requests.post(service_url,
                                 {"urls": json.dumps(filtered_url_list)}).json()

        if "error" in data:
            raise ValueError("cache could not obtain data. Error: %s" %
                             data["error"])
        else:
            url_keys = {}
            for pageData in data["response"]:
                target_field = ("content_bz2"
                                if "content_bz2" in pageData
                                   and pageData["content_bz2"]
                                else "content_raw_bz2")
                if target_field in pageData:
                    b64decoded = base64.b64decode(pageData[target_field][2:])
                    decompressed = bz2.decompress(b64decoded)
                    if target_field != "content_bz2":
                        print("we could not parse url %s into %s" %
                              (db_normalize_url(pageData["url_tuple"]), output))
                        pageData["content"] = decompressed
                    else:
                        pageData["content"] = pickle.loads(decompressed)
                    del pageData[target_field]
                    url_keys[pageData["url_key"]] = pageData
                else:
                    print("there was a problem processing URL %s" %
                          pageData["url"])

            return {
                url_item: url_keys.get(
                    db_normalize_url(url_item),
                    {"url": url_item, "content": None, "error": True}
                ) for url_item in url_list}


def is_valid_url(url):
    return type(url) == str and len(url.strip()) > 0 and url.startswith("http")


def db_normalize_url(url_item):
    the_url, the_data = ((url_item, {})
                         if isinstance(url_item, str)
                         else (url_item[0], json.loads(url_item[1])))
    try:
        # consider http and https as EQUAL for the key
        lower_link_furl = furl.furl(
            the_url.lower().strip().replace("https://", "http://"))

        lower_link_furl.path.normalize()
        lower_link_furl.query.params = sorted(
            [(par, lower_link_furl.query.params[par])
             for par in lower_link_furl.query.params],
            key=lambda item: item[0])
        lower_link_furl.path = ("%s/" % lower_link_furl.path
                                if not str(lower_link_furl.path).endswith("/")
                                else lower_link_furl.path)
        lower_link = lower_link_furl.url

        if the_data:
            data_json = json.dumps(the_data, sort_keys=True).lower()
            lower_link = f"{lower_link}_{data_json}"

        return lower_link
    except Exception:
        print("COULD NOT DB NORM URL %s" % the_url)
        return None
