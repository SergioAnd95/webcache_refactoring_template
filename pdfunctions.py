import datetime


class SkipURL(Exception):
    pass


def time_diff_to_now(previous_time):
    diff = (datetime.datetime.now() - previous_time)
    return diff.microseconds + diff.seconds * 10 ** 6
