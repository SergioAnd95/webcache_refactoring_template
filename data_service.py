#!flask/bin/python
import base64
import bz2
import json
import multiprocessing
import os
import pickle
import traceback
import re
from datetime import datetime, timedelta
from multiprocessing.pool import ThreadPool
from typing import Union

import pymongo
import requests
from bs4 import BeautifulSoup
from flask import Flask, jsonify, abort, make_response, request
from requests.exceptions import ProxyError, ConnectTimeout, SSLError, \
    ConnectionError, ChunkedEncodingError, ReadTimeout

from captcha_execution import CaptchaError
from pdfunctions import time_diff_to_now, SkipURL
from proxyhandling import DBProxyHandler
from webcacheclient import db_normalize_url, is_valid_url

app = Flask(__name__)

MONGO_LOCATION = "mongo"

FLASK_IP = "0.0.0.0"
# FLASK_IP = "10.5.133.201"
MAX_TIMES_FOR_URL = 20


@app.route(
    "/fetch/<int:max_age_days>/<string:category>"
    "/<string:output>/<string:method>",
    methods=["POST"])
def fetch_url(max_age_days, category, output="xml", method="GET"):
    try:
        if output.lower() not in ["xml", "json"]:
            raise ValueError(
                "we only support XML and JSON as output formats for now")

        urls = json.loads(request.form["urls"])
        urls = [url_tuple for url_tuple in urls if is_valid_url(url_tuple[0])]

        print("preparing to fetch data for %s urls.." % len(urls))
        the_data = {
            "response": list(
                get_data(urls, method, max_age_days, category, output))
            if urls else []}
        return make_response(jsonify(**the_data))

    except Exception as e:
        print(traceback.format_exc())
        abort(500, e)


@app.route("/proxies/<int:num_proxies>", methods=["GET"])
def get_proxies(num_proxies):
    client = pymongo.MongoClient(MONGO_LOCATION)
    db = client.webdata
    ph = DBProxyHandler(db)
    data = {"response": ph.pick(num_proxies)}
    return make_response(jsonify(**data))


def get_data(url_list: list, method: str,
             max_age_days: int, category, output="xml"):
    if method.upper() not in ['GET', 'POST']:
        raise ValueError("only GET/POST supported")

    url_data = {
        db_normalize_url(url_tuple):
            {"url_tuple": url_tuple, "url_key": db_normalize_url(url_tuple)}
        for url_tuple in url_list
    }

    db = pymongo.MongoClient(MONGO_LOCATION).webdata
    existing = {data["url_key"]: data for data in
                db.webpages.find(
                    {
                        "url_key": {"$in": list(url_data.keys())},
                        "format": output,
                        "creation_date": {
                            "$gt": datetime.now() - timedelta(days=max_age_days)
                        },
                    })}
    url_data.update(existing)

    # for those where we do not have data -> obtain
    url_keys_to_obtain = [url_key for url_key in url_data
                          if "format" not in url_data[url_key]]
    print("found %s entries already and will need "
          "to obtain an additional %s. Total Unique urls: %s" %
          (len(existing), len(url_keys_to_obtain), len(url_data)))
    if len(url_keys_to_obtain) > 0:
        global url_counter
        url_counter = {url_key: 0 for url_key in url_keys_to_obtain}
        chunk_len = int(len(url_keys_to_obtain) / os.cpu_count())
        chunk_len = chunk_len if chunk_len != 0 else 1
        processes = [
            multiprocessing.Process(target=process_url_chunk, args=(
                url_keys_to_obtain[x:x + chunk_len], url_data,
                method, output, category, max_age_days))
            for x in range(0, len(url_keys_to_obtain), chunk_len)
        ]
        for proc in processes:
            proc.start()

        for proc in processes:
            proc.join()
        client = pymongo.MongoClient(MONGO_LOCATION)
        db = client.webdata
        remaining_data = {data["url_key"]: data for data in db.webpages.find(
            {
                "url_key": {"$in": list(url_data.keys())},
                "format": output,
                "creation_date": {
                    "$gt": datetime.now() - timedelta(days=max_age_days)}
            })}
        client.close()
        url_data.update(remaining_data)

    print("finished obtaining data, encoding everything and returning it.")

    for data in url_data:
        target_field = ("content_bz2"
                        if "content_bz2" in url_data[data]
                           and url_data[data]["content_bz2"]
                        else "content_raw_bz2")

        if target_field in url_data[data]:
            url_data[data][target_field] = str(
                base64.b64encode(url_data[data][target_field]))
        else:
            url_data[data]["error"] = "could not obtain address!"
            url_data[data]["content_raw_bz2"] = ""
        if "_id" in url_data[data]:
            del url_data[data]["_id"]
    return url_data.values()


def process_url_chunk(chunk, url_data, method, output, category, max_age_days):
    print("process started for %s URL's in category %s" %
          (len(chunk), category))

    if not chunk:
        return

    with ThreadPool(min(100, len(chunk))) as thread_pool:
        thread_pool.starmap(
            try_n_times_to_get_page,
            [
                (url_key, url_data[url_key]["url_tuple"], method, output,
                 category, max_age_days) for url_key in chunk
            ])
        print("process finished %s URL's in category %s" %
              (len(chunk), category))


def update_db_entry(result, url_tuple, n_tries=2):
    client = pymongo.MongoClient(MONGO_LOCATION)
    db = client.webdata
    if n_tries < 0:
        return None
    try:
        db.webpages.replace_one(
            {"url_key": db_normalize_url(url_tuple)},
            result, upsert=True)
    except pymongo.errors.AutoReconnect:
        print("pymongo error: could not auto reconnect")
        update_db_entry(result, n_tries - 1)
    client.close()


def try_n_times_to_get_page(
    url_key: str, url_tuple: tuple, method: str,
    output: str, category: str, max_age_days, tries=0, multiprocessed=False
):
    with pymongo.MongoClient(MONGO_LOCATION) as client:
        db = client.webdata
        finished = {data["url_key"]: data for data in
                    db.webpages.find(
                        {
                            "url_key": {"$in": [url_key]},
                            "format": output,
                            "creation_date": {
                                "$gt": (datetime.now()
                                        - timedelta(days=max_age_days))
                            }
                        }
        )}
    if finished:
        return
    print("attempt %s to get URL %s" % (tries, db_normalize_url(url_tuple)))
    if url_counter[url_key] >= MAX_TIMES_FOR_URL:
        print("I'm giving up fetching URL %s" % db_normalize_url(url_tuple))
        return
    with pymongo.MongoClient(MONGO_LOCATION) as client:
        db = client.webdata
        ph = DBProxyHandler(db)
        proxy = ph.pick()
        try:
            result = obtain_page(url_tuple, method, output, proxy)
            result["category"] = category
            ph.feedback(proxy, 1)
            update_db_entry(result, url_tuple)
            return
        except (ProxyError, ConnectTimeout, SSLError,
                ConnectionError, ReadTimeout, ChunkedEncodingError,
                CaptchaError):
            ph.feedback(proxy, -1)
            if not multiprocessed:
                with ThreadPool(5) as pool:
                    new_queries = []
                    for i in range(1, 6):
                        url_counter[url_key] = url_counter[url_key] + 1
                        new_queries.append(
                            (url_key, url_tuple, method, output, category,
                             max_age_days, url_counter[url_key], True))
                    url_counter[url_key] = url_counter[url_key] + 5
                    pool.starmap(try_n_times_to_get_page, new_queries)
            else:
                url_counter[url_key] = url_counter[url_key] + 1
                try_n_times_to_get_page(url_key, url_tuple, method, output,
                                        category, max_age_days,
                                        url_counter[url_key], True)
        except Exception:
            print("encountered exception on url %s: %s" %
                  (db_normalize_url(url_tuple), traceback.format_exc()))
            return


def obtain_page(url_tuple: tuple, method: str, output: str, proxy: str):
    url, data_json = url_tuple[0], url_tuple[1]
    headers = {
        "Accept": "text/html,application/"
                  "xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        'User-Agent': "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_7;en-us) "
                      "AppleWebKit/530.17 (KHTML, like Gecko) "
                      "Version/4.0 Safari/530.17"
    }
    download_start_time = datetime.now()
    requests.packages.urllib3.disable_warnings()

    with requests.request(
        method, url,
        data=json.loads(data_json), headers=headers,
        verify=False, stream=True,
        proxies={"https": proxy, "http": proxy},
        timeout=60
    ) as req:

        maxsize = 5e6  # max size = 5MB. Will stop afterwards
        data = b''
        encountered_size_limit = False
        parsed_data = None
        try:
            for chunk in req.iter_content(2048):
                data += chunk
                if len(data) > maxsize:
                    raise SkipURL("too much data. I'm limited to %s bytes"
                                  % maxsize)
            try:
                interpreter = json.loads(
                    data.decode()) \
                    if output.lower() == "json" else BeautifulSoup(data, "lxml")

                if has_captcha(interpreter):
                    raise CaptchaError('Captcha response detected.')
                parsed_data = bz2.compress(pickle.dumps(interpreter))
            except Exception:
                pass  # if we cannot parse the data..
        except SkipURL:
            encountered_size_limit = True
            pass

        to_return = {
            "download_duration_ms": time_diff_to_now(download_start_time),
            "content_bz2": parsed_data,
            "size": len(data),
            "url_tuple": url_tuple, "format": output,
            "url_key": db_normalize_url(url_tuple),
            "creation_date": datetime.now()}
        if encountered_size_limit:
            to_return["cancelled"] = "size limit"
        if parsed_data is None:
            to_return["cancelled"] = "parsing error"
            to_return["content_raw_bz2"] = bz2.compress(data)

        return to_return


def has_captcha(response: Union[json.JSONEncoder, BeautifulSoup]) -> bool:
    recaptcha_url = re.compile('https?://www.google.com/recaptcha/api.*')
    if isinstance(response, json.JSONEncoder):
        pass
    elif isinstance(response, BeautifulSoup):
        script_tags = response.find_all('script')
        for script in script_tags:
            try:
                if recaptcha_url.match(script['src']):
                    return True
            except KeyError:
                pass
        iframe_tags = response.find_all('iframe')
        for iframe in iframe_tags:
            try:
                if recaptcha_url.match(iframe['src']):
                    return True
            except KeyError:
                pass
    return False


@app.errorhandler(500)
def not_found(error):
    return make_response(
        jsonify({"error": str(error), "traceback": str(error.__traceback__)}))


if __name__ == '__main__':
    client = pymongo.MongoClient(MONGO_LOCATION)
    db = client.webdata
    db.webpages.create_index([('url_key', pymongo.ASCENDING)], unique=True)
    db.webpages.create_index([('creation_date', pymongo.ASCENDING)])
    client.close()
    url_counter = {}
    app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True
    app.run(host=FLASK_IP, port=9011, debug=True)
